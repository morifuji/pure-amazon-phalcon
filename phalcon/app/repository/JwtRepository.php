<?php

namespace App\Repository;

use Firebase\JWT\JWT;
use Phalcon\Security\Random;

/**
 * Class JwtRepository
 * @package App\Repository
 */
class JwtRepository
{

    const DEFAULT_PARAMS = [
        "iss" => "saitan",
        "aud" => "saitan",
    ];

    const EXP_TIME = "+ 30 minutes";

    const PRIVATE_KEY_START_LABEL = "-----BEGIN RSA PRIVATE KEY-----";
    const PRIVATE_KEY_END_LABEL = "-----END RSA PRIVATE KEY-----";
    const PUBLIC_KEY_START_LABEL = "-----BEGIN PUBLIC KEY-----";
    const PUBLIC_KEY_END_LABEL = "-----END PUBLIC KEY-----";

    /**
     * tokenを作成する
     * @return mixed
     */
    public static function createToken($username, $role)
    {
        $random = new Random();

        $token = [
                "exp" => strtotime(self::EXP_TIME),
                "jti" => $random->hex(10),
                "username" => $username,
                "role" => $role,
            ] + self::DEFAULT_PARAMS;

        $privateKey = self::PRIVATE_KEY_START_LABEL
            . PHP_EOL
            . getenv("JWT_PRIVATE_KEY")
            . PHP_EOL
            . self::PRIVATE_KEY_END_LABEL;

        $jwt = JWT::encode($token, $privateKey, 'RS256');

        return $jwt;
    }

    /**
     * デコードする
     * @param string $jwt encodedなtoken
     * @return object
     */
    public static function decodeToken($jwt)
    {
        $publicKey = self::PUBLIC_KEY_START_LABEL
            . PHP_EOL
            . getenv("JWT_PUBLIC_KEY")
            . PHP_EOL
            . self::PUBLIC_KEY_END_LABEL;

        $decoded = JWT::decode($jwt, $publicKey, ['RS256']);
        return $decoded;
    }
}