<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerDirs(
    [
        $config->application->modelsDir
    ]
)->registerNamespaces([
    'App\Controllers' => __DIR__ . '/../controllers/',
    'App\Controllers\V1' => __DIR__ . '/../controllers/v1/',
    'App\Repository' => __DIR__ . '/../repository/',
])->register();
