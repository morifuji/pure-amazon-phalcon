<?php
/**
 * Created by PhpStorm.
 * User: civiluo-1
 * Date: 2018/04/30
 * Time: 1:35
 */

use Phalcon\Acl;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Acl\Resource;

$acl = new AclList();

// Default action is deny access
$acl->setDefaultAction(
    Acl::DENY
);

// Add 'Guests' role to ACL
$acl->addRole('guest');
$acl->addRole('user', 'guest');
$acl->addRole('admin', 'user');

// Define the 'Customers' resource
$customersResource = new Resource('Customers');

$acl->addResource(
    $customersResource,
    'search'
);

$acl->allow('guest', 'Customers', 'search');

return $acl;