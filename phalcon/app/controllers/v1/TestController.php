<?php

namespace App\Controllers\V1;

/**
 * Class TestController
 */
class TestController extends BaseV1Controller
{
    /**
     * @return \Phalcon\Http\Response
     */
    public function index()
    {
        return $this->success($this->auth)->send();
    }

    /**
     * @param $name
     */
    public function show($name)
    {
        $this->response->send();
        return;
    }
}