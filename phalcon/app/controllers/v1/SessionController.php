<?php

namespace App\Controllers\V1;

use App\Repository\JwtRepository;
use Phalcon\Crypt;
use Users;


/**
 * Class SessionController
 * @package App\Controllers\V1
 */
class SessionController extends BaseV1Controller
{
    /**
     * ログイン処理
     */
    public function login()
    {
        if ($this->auth->role !== 'guest') {
            $this->error(500)->send();
            return;
        }

        $username = $this->request->getPost("username");
        $password = $this->request->getPost("password");
        $encryptPassword = (new Crypt())->encryptBase64($password, getenv("DB_CRYPT_KEY"));

        $user = Users::findFirstByUsername($username);
        if (!$user || $user->password !== $encryptPassword) {
            $this->error()->send();
            return;
        }

        $jwt = JwtRepository::createToken("hogehoge", "admin");
        $this->success()->setHeader("Authorization", "Bearer $jwt")->send();
    }


    /**
     * 会員登録
     */
    public function signup()
    {
        if ($this->auth['role'] !== 'guest') {
            $this->error(500)->send();
            return;
        }

        $param = $this->getJsonParam();

        $user = new Users([
            'username' => $param->username,
            'email' => $param->username,
            'password' => (new Crypt())->encryptBase64($param->password, getenv("DB_CRYPT_KEY")),
            'role' => 'user'
        ]);

        if (!$user->validation()) {
            return $this->error()->send();
        }

        $user->create();

        // 同時にログイン
        $jwt = JwtRepository::createToken($user->username, "user");
        $this->success()->setHeader("Authorization", "Bearer $jwt")->send();
    }
}