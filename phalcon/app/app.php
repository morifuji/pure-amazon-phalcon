<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */
use App\Controllers\BaseController;
use App\Repository\JwtRepository;
use Phalcon\Events\Event;
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Phalcon\Mvc\Router;
use Phalcon\Session\Adapter\Files as Session;
use Phalcon\Events\Manager as EventsManager;

// セッションの初期設定
$di->setShared(
    'session',
    function () {
        $session = new Session();
        $session->start();
        return $session;
    }
);

$router = new Router();

//// JWTログイン確認
//$eventsManager = new EventsManager();
//$eventsManager->attach(
//    'micro:beforeExecuteRoute',
//    function (Event $event, $app) {
//        if (!$app->session->get('auth')) {
//            $controller = new BaseController();
//            $controller->error('not_login');
//            return false;
//        }
//    }
//);
//$app->setEventsManager($eventsManager);

$v1Namespace = "App\Controllers\V1\\";
$test = new MicroCollection();
$test->setHandler($v1Namespace . 'TestController', true);
$test->setPrefix('/test');
$test->get('/', 'index');
$test->get('/hoge/{name}', 'show');
$app->mount($test);


$session = new MicroCollection();
$session->setHandler($v1Namespace . 'SessionController', true);
$session->setPrefix('/session');
$session->get('/login', 'login');
$session->post('/signup', 'signup');
$app->mount($session);

/**
 * notfoundとエラー時を定義
 * TODO インスタンス作成せずにスマートにできないか
 */
$app->notFound(
    function () use ($app) {
        $controller = new BaseController();
        return $controller->error(404)->send();
    }
);
$app->error(
    function (Exception $exception) {
        var_dump($exception->getMessage());
        var_dump($exception->getTrace());
        $controller = new BaseController();
        return $controller->error(500)->send();
    }
);

$app->setService('router', $router, true);
