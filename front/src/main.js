// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import Buefy from 'buefy'
import axios from 'axios'
import 'buefy/lib/buefy.css'

import 'bulma-extensions/bulma-calendar/dist/bulma-calendar.min.css'

import InstantSearch from 'vue-instantsearch'

Vue.use(Buefy)
Vue.use(InstantSearch)

Vue.use(Vuex)

Vue.config.productionTip = false

// ストアパターン採用
const store = new Vuex.Store({
  state: {
    // 読み込み中かどうか
    isLoading: false,
    // 叩かれたURL
    path: ''
  },
  mutations: {
    stopLoading (state) {
      state.isLoading = false
    },
    startLoading (state) {
      state.isLoading = true
    }
  },
  getters: {
    isLoading (state) { return state.isLoading }
  },
  actions: {
    post (context, json) {
      return axios.post('http://localhost:8111/session/signup', json).then(function (response) {
        console.log(response)
      }, {
        headers: { 'Content-Type': 'application/json' }
      }).catch(function (error) {
        console.log(error)
      })
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  store: store
})
