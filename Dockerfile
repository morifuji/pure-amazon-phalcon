FROM amazonlinux:2

RUN yum update -y
RUN yum upgrade -y

#タイムゾーン設定
# RUN timedatectl set-timezone Asia/Tokyo

# 日本語設定
RUN yum install -y glibc-langpack-ja
RUN export LANG='ja_JP.utf8'

# nginxインストール&起動
RUN amazon-linux-extras install nginx1.12
RUN nginx

# phpインストール
RUN amazon-linux-extras install php7.2

# php-fpmインストール
RUN yum install -y php-fpm

# php-fpm設定
RUN sed -i 's/user\ \=\ apache/user = nginx/g' /etc/php-fpm.d/www.conf
RUN sed -i 's/group\ \=\ apache/group = nginx/g' /etc/php-fpm.d/www.conf

# nginxの設定
# docker-composeで共有するのでコメントアウト
# ADD ./nginx.conf /etc/nginx/nginx.conf

# コンパイルに必要
RUN yum install -y php-devel pcre-devel gcc make

# gitからphalconビルドソースを取得
RUN yum install -y git
RUN git clone --depth=1 "git://github.com/phalcon/cphalcon.git"

# ビルド中に必要と言われるが、yumでは取得できないのでコメントアウト
#RUN yum install -y re2c

# ビルド。めっちゃ時間かかる
RUN cd cphalcon/build && \    
    ./install

# extension追加
RUN echo "extension=phalcon.so" > /etc/php.d/phalcon.ini

# composerを使ってdev-toolsをインストール
RUN curl -s http://getcomposer.org/installer | php
RUN echo '{"require": {"phalcon/devtools": "dev-master"}}' > composer.json

# composerインストール時に必要
RUN yum install -y zip unzip
RUN php composer.phar install

# シンボリックリンク作成
RUN ln -s /vendor/phalcon/devtools/phalcon.php /usr/bin/phalcon

# 権限の設定。今のところ不必要なのでコメントアウト
#RUN chmod ugo+x /usr/bin/phalcon

RUN mkdir /var/www
RUN chown -R nginx /var/www

# 汎用的なDockerimageのため、プロジェクトは作らない
#　RUN cd /var/www && phalcon project phalcon --enable-webtools

# 起動設定
CMD /bin/bash -c "nginx && php-fpm && tail -f /dev/null"

